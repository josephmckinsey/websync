import click
import toml
import os
import urllib.request
import urllib.error
from hashlib import blake2b
import time
import bs4
from pathlib import Path
name_hashing = blake2b(key=b'webchecker', digest_size=4)


@click.group(help='Update text')
@click.option('--config', default='~/.config/websync')
@click.pass_context
def cli(ctx, config):
    ctx.ensure_object(dict)
    config = os.path.expanduser(config)
    os.makedirs(config, exist_ok=True)
    os.chdir(config)
    if os.path.exists('config.toml'):
        ctx.obj['config'] = toml.load('config.toml')
    else:
        ctx.obj['config'] = {}


def download(url):
    request = urllib.request.Request(url, headers={'User-Agent': 'Mozille/5.0'})
    try:
        return urllib.request.urlopen(request)
    except urllib.error.HTTPError as e:
        return e


@cli.command()
@click.argument('url')
@click.option('--name', default=None, help='filename to save under')
@click.option('--update', default='everytime', help='how often to sync',
              type=click.Choice(['everytime', 'hourly', 'daily', 'weekly'],
                  case_sensitive=False))
@click.pass_context
def new(ctx, url, name, update):
    config = ctx.obj['config']
    if name is None:
        name_hashing.update(bytes(url, 'utf'))
        name = name_hashing.hexdigest()
    print(name)
    config[name] = {'url': url, 'update': update}
    request = download(url)
    if request.getcode() != 200:
        return
    with open(name, 'wb') as f:
        f.write(request.read())
    with open('config.toml', 'w') as f:
        toml.dump(config, f)

    
@cli.command()
@click.argument('name')
@click.pass_context
def delete(ctx, name):
    if name not in ctx.obj['config']:
        return
    print(f'Deleting {name}')
    del ctx.obj['config'][name]
    if os.path.exists(name):
        os.remove(name)
    with open('config.toml', 'w') as f:
        toml.dump(ctx.obj['config'], f)


def should_update(minutes, update):
    if update == 'everytime':
        return True
    if update == 'hourly':
        if minutes >= 60:
            return True
        return False
    if update == 'daily':
        if minutes >= 60*24:
            return True
        return False
    if update == 'weekly':
        if minutes >= 60*24*7:
            return True
        return False
    return False


def similar(old_data, data, mimetype):
    if 'html' in mimetype or 'xml' in mimetype:
        old_html = bs4.BeautifulSoup(old_data, 'html.parser')
        html = bs4.BeautifulSoup(data, 'html.parser') 
        return ((old_html.find_all('body') == html.find_all('body')) and
                (old_html.find_all('item') == html.find_all('item')))
    return old_data == data

@cli.command()
@click.argument('names', nargs=-1)  #, help='List of names to update')
@click.option('--force', '-f', is_flag=True, help='Update no matter the policy')
@click.pass_context
def update(ctx, names, force):
    config = ctx.obj['config']
    if names == ('all',) or names == ():
        names = config.keys()
    for name in names:
        if os.path.exists(name) and not force:
            minutes = (time.time() - os.path.getmtime(name)) / 60
            if not should_update(minutes, config[name]['update']):
                continue
        print(f"Checking {name}: {config[name]['url']}")
        request = download(config[name]['url'])
        mimetype = request.headers['content-type']
        if request.getcode() == 200:
            data = request.read()
            if os.path.exists(name):
                with open(name, 'rb') as f:
                    old_data = f.read()
                if similar(old_data, data, mimetype):
                    Path(name).touch()
                    continue
            with open(name, 'wb') as f:
                f.write(data)
            print(f"Updated {name}: {config[name]['url']}")


def main():
    cli(obj={})

