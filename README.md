# websync

Do you hate RSS feeds like I do? Do you want extra privacy, but also value simplicity?

Well, websync is designed to check RSS feeds as simply as possible.

- Use `websync new [URL]` to add a URL. I would suggest adding a name with `--name` so it's easier to manage later. You can also use `--update` so that you don't update too often.
- Use `websync update` to check all your URLs for updates in their bodies. With luck, this will work with both HTML and RSS feeds.
