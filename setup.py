import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "websync",
    version = "0.0.1",
    author = "Joseph McKinsey",
    author_email = "me@josephmckinsey.com",
    description = ("I don't like using RSS feed websites, so I have this program. "
        "It manages a TOML file of websites along with a cache, then it "
        "checks them once in an while and updates based on config options."),
    license = "MIT",
    keywords = "cacheing RSS",
    url = "http://gitlab.com/josephmckinsey/websync",
    packages=['websync'],
    scripts=['./scripts/websync'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.7',
    install_requires=[
        'click==*',
        'toml==*',
        'beautifulsoup4==4.8.*'
    ]
)
